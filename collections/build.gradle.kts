description = "Provides utilities/extensions and data structures for working with collections in Kotlin"
version = "0.3.0"
extra["packageName"] = "collections"