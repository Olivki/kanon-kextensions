group = "moe.kanon.kommons"
description = "The core library used by all the kanon.kommons modules"
version = "1.0.0"
extra["packageName"] = "core"